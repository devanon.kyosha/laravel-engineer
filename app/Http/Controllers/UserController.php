<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    private $model_user;
    function __construct() {
        $this->model_user = new User();
    }
    public function setData(Request $request) {
        $data = [
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ];
        return $data;
    }
    public function index()
    {
        $users = $this->model_user->getAllUsers();
        return response()->json($users);
    }
    public function create()
    {
        return view('user.create');
    }
    public function store(Request $request)
    {
        $id = $this->model_user->store($this->setData($request));
        $user = $this->model_user->getUser($id);
        return response()->json($users);
    }
    public function show($id)
    {
        $user = $this->model_user->getUser($id);
        return response()->json($user);
    }
    public function edit($id)
    {
        $user = $this->model_user->getUser($id);
        return view('user.edit', compact('user'));
    }
    public function update(Request $request, $id)
    {
        $this->model_user->updateWingoutModel($id,$this->setData($request));
        $user = $this->model_user->getUser($id);
        return response()->json($user);   
    }
    public function destroy($id)
    {
        $this->model_user->remove($id);
        return response()->json('Usuário eliminado com sucesso'); 
    }
}
