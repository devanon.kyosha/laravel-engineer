<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'document',
        'city',
        'state',
        'start_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
    public function getAllEmployees()
    {
        return DB::connection('sqlite')->table('employees as e')
               ->join('users as u', 'u.id', '=', 'e.user-id')
                ->select('e.*', 'u.*')->get();
    }
    public function remove($id){
        Employee::where('id', '=' ,$id)->delete();
    }
    public function store(array $options = [])
    {
        return Employee::query()->insertGetId($options);
    }
    public function getEmployee($id)
    {
        return Employee::query()->select('*')->where('id', '=', $id)->first();
    }
    public function updateWingoutModel($id, Array $options)
    {
        Employee::query()->where('id', '=', $id)->update($options);
    }
}
