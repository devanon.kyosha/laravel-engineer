<?php

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});
Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'
        ], function ($router) {

            Route::post('login', 'AuthController@login')->name('login');
            Route::post('logout', 'AuthController@logout')->name('logout');
            Route::post('refresh', 'AuthController@refresh')->name('refresh');
            Route::post('me', 'AuthController@me')->name('me');
        });

//Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::post('/register', [UserController::class, 'store'])->name('register');
Route::get('/api/user', function () {
    return view('auth.login');
});
Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('/get', [EmployeeController::class, 'get'])->name('get');
    Route::get('/show', [EmployeeController::class, 'show'])->name('show');
    Route::get('/destroy', [EmployeeController::class, 'destroy'])->name('destroy');
});
